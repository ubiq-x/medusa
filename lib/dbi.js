/**
 * A rudimentary Database interface (DBI).
 */

var fs = require("fs"),
    os = require("os"),
    pg = require("pg");

var PRIVATE = require(process.cwd() + "/lib/private.js");

exports.CONN_STR_DB        = PRIVATE.DB_CONN_STR + "/medusa";
exports.CONN_STR_TEMPLATE1 = PRIVATE.DB_CONN_STR + "/template1";


// -----------------------------------------------------------------------------------------------------------------------------------
exports.close = function (client, doTrans, doCommit, fnCommit, fnEnd) {
  if (doTrans) {
    if (fnCommit) fnCommit();
    //exports.execQry(client, (doCommit ? "COMMIT;" : "ROLLBACK;")).on("end", function () {
    exports.execQry(client, (doCommit ? "COMMIT;" : "ROLLBACK;"), function (err, res) {
      client.end();
      if (fnEnd) fnEnd();
    });
  }
  else {
    client.end();
    if (fnEnd) fnEnd();
  }
};


// -----------------------------------------------------------------------------------------------------------------------------------
exports.escArr = function (a) { return a.map(exports.escStr); };
exports.escStr = function (s) { return "'" + s.replace(/'/g, "''") + "'"; };

//exports.escStr = function (s) { return "E'" + s.replace(/'/g, "\\'") + "'"; };


// -----------------------------------------------------------------------------------------------------------------------------------
exports.execQry = function (client, qry, fnCb) {
  return client.query(qry, function(err, res) {
    //done();  // release connection back to the pool
    if (err) {
      console.log(qry);
      console.log(err);
      fs.writeFile(process.cwd() + "/qry.err", qry, function (err) {
        console.error("Error excuting a query. It has been written to 'qry.err' for reference.");
        if (fnCb) fnCb(err, res);
      });
    }
    else {
      if (fnCb) fnCb(err, res);
    }
  });
};


// -----------------------------------------------------------------------------------------------------------------------------------
exports.getId = function (client, qrySel, qryIns, fnCb) {
  var fnDone = function (id) {
    if (fnCb) fnCb(parseInt(id));
  };

  exports.execQry(client, qrySel, function (err, res) {
    var id = (res && res.rows[0] ? res.rows[0].id : null);
    if (id === null || id === undefined || id.length === 0) {
      qryIns = (qryIns.substr(qryIns.length - 1) === ";" ? qryIns = qryIns.substr(0, qryIns.length - 1) : qryIns) + " RETURNING id;";
      exports.execQry(client, qryIns, function (err, res) { fnDone(res.rows[0].id); });
    }
    else fnDone(id);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
exports.getInt = function (client, qry, colName, fnCb) {
  exports.execQry(client, qry, function (err, res) {
    if (fnCb) fnCb(err, res, (res && res.rows[0] ? res.rows[0][colName] : null));
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
exports.open = function (doTrans, connStr) {
  var client = new pg.Client(connStr || exports.CONN_STR_DB);
  client.connect(function(err) {
    if (err) return console.error("Could not connect to the DB server.", err);
    if (doTrans) exports.execQry(client, "BEGIN;");
  });
  return client;
};


// -----------------------------------------------------------------------------------------------------------------------------------
exports.strToSql = function (s) { return s.replace(/[\s-.]/, "_"); };
