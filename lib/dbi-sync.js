/**
 * A rudimentary Database interface (DBI), the synchronous version.
 */

var fs = require("fs"),
    os = require("os"),
    pg = require("pg-native");

var PRIVATE = require(process.cwd() + "/lib/private.js");

exports.CONN_STR_DB        = PRIVATE.DB_CONN_STR + "/medusa";     // ?ssl=true
exports.CONN_STR_TEMPLATE1 = PRIVATE.DB_CONN_STR + "/template1";  // ?ssl=true


// -----------------------------------------------------------------------------------------------------------------------------------
exports.execQry = function (qry) {
  var c = new pg();
  c.connectSync(exports.CONN_STR_DB);
  var res = c.querySync(qry);
  c.end();
  return res;
};
