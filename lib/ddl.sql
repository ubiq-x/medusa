-- This script is executed by the application and that's why the three first commands are commented
-- out.

--DROP DATABASE IF EXISTS "medusa";
--CREATE DATABASE "medusa" WITH ENCODING 'UTF8' TEMPLATE=template0;

--\c "medusa"

BEGIN;


----------------------------------------------------------------------------------------------------
--DROP TABLE IF EXISTS question;
--DROP TABLE IF EXISTS chapter;
--DROP TABLE IF EXISTS txt;


----------------------------------------------------------------------------------------------------
-- Reset sequences (TODO)


----------------------------------------------------------------------------------------------------
CREATE TABLE txt (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  lang text NOT NULL,
  title text NOT NULL,
  title_en text NOT NULL,
  author text NOT NULL,
  year smallint NOT NULL CHECK (year >= 0),
  note text,
  is_editable boolean NOT NULL DEFAULT false,
  is_visible boolean NOT NULL DEFAULT true,
  UNIQUE(lang, title, author, year),
  UNIQUE(lang, title_en, author, year)
);

-- COMMENT ON COLUMN exp.name IS 'blah blah...';


----------------------------------------------------------------------------------------------------
CREATE TABLE chapter (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  txt_id bigint NOT NULL REFERENCES txt(id) ON DELETE CASCADE ON UPDATE CASCADE,
  num smallint CHECK (num >= 0),
  name text,
  content text NOT NULL,
  UNIQUE(txt_id, num),
  UNIQUE(txt_id, name)
);

DROP INDEX IF EXISTS chapter_txt_id;  CREATE INDEX chapter_txt_id ON chapter(txt_id);


----------------------------------------------------------------------------------------------------
CREATE TABLE question (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  created timestamp NOT NULL DEFAULT now(),
  chapter_id bigint NOT NULL REFERENCES chapter(id) ON DELETE CASCADE ON UPDATE CASCADE,
  q text NOT NULL,
  a1 text NOT NULL,
  a2 text NOT NULL,
  a3 text NOT NULL,
  a4 text NOT NULL,
  word_a smallint CHECK (word_a >= 0),
  word_b smallint CHECK (word_b >= word_a)
);

DROP INDEX IF EXISTS question_chapter_id;  CREATE INDEX question_chapter_id ON question(chapter_id);

COMMENT ON COLUMN question.q      IS 'The question';
COMMENT ON COLUMN question.a1     IS 'Answer: Correct';
COMMENT ON COLUMN question.a2     IS 'Answer: Incorrect (i.e., a distractor)';
COMMENT ON COLUMN question.a3     IS 'Answer: Incorrect (i.e., a distractor)';
COMMENT ON COLUMN question.a4     IS 'Answer: Incorrect (i.e., a distractor)';
COMMENT ON COLUMN question.word_a IS 'The first word from the range that answers the question';
COMMENT ON COLUMN question.word_b IS 'The last word from the range that answers the question';


----------------------------------------------------------------------------------------------------
COMMIT;
