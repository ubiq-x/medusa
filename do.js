/*
BSD 3-Clause License

Copyright (c) 2018, Tomek D. Loboda
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */


/* ******************************************************************************************************
 *
 * Text annotation application with a console and a HTTP interface.  These two interface expose
 * different functionalities.
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * INSTALLATION:
 *
 * 1. Download and install the latest node.js.
 * 2. Update NPM:
 *    npm install npm@latest -g
 *    # sudo npm install npm -g
 * 3. Install the required node.js packages:
 *    npm install fs, http, moment, path, pg, pg-native, pushover-notifications, readline, request, shelljs, text-table, url
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * TODO:
 *
 * - ...
 *
 * ------------------------------------------------------------------------------------------------------
 *
 * RES:
 *
 * - ...
 *
 * ******************************************************************************************************/


// ----[ CONST ]----------------------------------------------------------------------------------------------------------------------

var DIR_LIB               = process.cwd() + "/lib",
    DIR_LOG               = process.cwd() + "/log",
    DIR_WORK              = process.cwd() + "/work",
    DIR_STIM              = process.cwd() + "/stim",
    PRIVATE               = require(DIR_LIB + "/private.js"),
    DB_NAME               = "medusa",
    SERVER_PORT           = "9999",
    PUSHOVER_USR_KEY      = PRIVATE.PUSHOVER_USR_KEY || "",
    PUSHOVER_APP_KEY      = PRIVATE.PUSHOVER_APP_KEY || "",
    PUSHOVER_DEV          = PRIVATE.PUSHOVER_DEV     || "",
    PUSHOVER_SND          = PRIVATE.PUSHOVER_SND     || "",
    PUSHOVER_HOST         = PRIVATE.PUSHOVER_HOST    || "",
    VALID_FILE_EXT        = {
      ".html" : "text/html",
      ".js"   : "application/javascript",
      ".css"  : "text/css",
      ".txt"  : "text/plain",
      ".jpg"  : "image/jpeg",
      ".gif"  : "image/gif",
      ".png"  : "image/png",
      ".ttf"  : "image/ttf",
      ".ico"  : "image/x-icon",
      ".mp4"  : "video/mp4",
      ".zip"  : "application/zip"
    },

    IMG_TXT_CHAPTER       = { en: "Chapter", sp: "Capítulo" },
    IMG_TXT_AA            = { en: "I don't know, and I don't even remember reading about that", sp: "No sé, y no recuerdo haber leído sobre eso" },
    IMG_TXT_AB            = { en: "I don't know, but I do remember reading about that", sp: "No sé, pero recuerdo haber leído sobre eso" },
    IMG_TXT_AC            = { en: "I know that from previous chapters", sp: "Sé la respuesta por información en capítulos anteriores" };


// ----[ LIB ]------------------------------------------------------------------------------------------------------------------------

//require("string.prototype.endswith");

var fs       = require("fs"),
    http     = require("http"),
    moment   = require("moment"),
    os       = require("os"),
    path     = require("path"),
    push     = require("pushover-notifications"),
    readline = require("readline"),
    request  = require("request"),
    sh       = require("shelljs"),
    tt       = require("text-table"),
    url      = require("url"),

    dbi      = require(DIR_LIB + "/dbi.js"),
    dbiSync  = require(DIR_LIB + "/dbi-sync.js"),
    timer    = require(DIR_LIB + "/timer.js");


// ----[ VAR ]------------------------------------------------------------------------------------------------------------------------

var selfName = path.basename(process.argv[1]);  // the name of this very file (in case it changes)


// ----[ NODE ]-----------------------------------------------------------------------------------------------------------------------



// ----[ CODE ]-----------------------------------------------------------------------------------------------------------------------

function chapterGetContent(resHttp, chapterId) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "SELECT content FROM chapter WHERE id = " + chapterId, function (err, result) {
    if (resHttp) {
      serverSend(resHttp, (result.rows.length === 0 ? "{ok:false,res:null,msg:\"Chapter not found.\"}" : "{ok:true,res:" + utilEscStr(result.rows[0].content) + "}" ));
    }
    else {
      console.log(result.rows.length === 0 ? "null" : result.rows[0].content);
    }

    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function chapterLst(resHttp, txtId, doGetContent) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "SELECT c.id, c.num, c.name, " + (doGetContent ? "c.content, " : "") + "array_length(string_to_array(c.content, ' '), 1) AS word_cnt, COALESCE(qq.cnt, 0) AS question_cnt FROM chapter c INNER JOIN txt t ON c.txt_id = t.id LEFT JOIN (SELECT chapter_id, COUNT(id) AS cnt FROM question GROUP BY chapter_id) qq ON qq.chapter_id = c.id WHERE t.id = " + txtId + " ORDER BY c.num;", function (err, result) {
    if (resHttp) {
      var js = "{ok:true,res:[" + result.rows.map(
        function (x) {
          return "{id:" + x.id + ",num:" + x.num + ",name:" + utilEscStr(x.name) + (doGetContent ? ",content:" + utilEscStr(x.content.replace(/\n/g, "\\n")) : "") + ",wordCnt:" + x.word_cnt + ",questionCnt:" + x.question_cnt + "}";
        }
      ).join(",") + "]}";

      serverSend(resHttp, js);
    }
    else {
      var t = tt(result.rows.map(
        function (x) {
          return [ x.id, x.num, x.name, (doGetContent ? x.content : ""), x.word_cnt, x.question_cnt ];
        }
      ));

      console.log(t);
    }

    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function chapterUpdContent(resHttp, chapterId, content) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "UPDATE chapter SET content = " + dbi.escStr(content) + " WHERE id = " + chapterId + ";", function (err, res) {
    if (resHttp) serverSend(resHttp, "{ok:true}");
    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function dbInit() {
  utilGetUserConfirm("Type 'INIT' to initialize the database (if one exists already, it will be destroyed):", "init",
    function () {
      timer.start("dbInit");
      var dbc = dbi.open(false, dbi.CONN_STR_TEMPLATE1);
      dbi.execQry(dbc, "DROP DATABASE IF EXISTS \"" + DB_NAME + "\";", function (err, res) {
        dbi.execQry(dbc, "CREATE DATABASE \"" + DB_NAME + "\" WITH ENCODING 'UTF8' TEMPLATE=template0;", function (err, res) {
      //dbi.execQry(dbc, "DROP TABLE IF EXISTS question; DROP TABLE IF EXISTS page; DROP TABLE IF EXISTS chater; DROP TABLE IF EXISTS txt;", function (err, res) {
      //  dbi.execQry(dbc, "DROP TABLE IF EXISTS question; DROP TABLE IF EXISTS page; DROP TABLE IF EXISTS chater; DROP TABLE IF EXISTS txt;", function (err, res) {
          dbi.close(dbc, false);

          var dbc2 = dbi.open(false);
          dbi.execQry(dbc2, fs.readFileSync(DIR_LIB + "/ddl.sql").toString(), function (err, res) {
            //dbi.close(dbc, true, true, null, function () {
            dbi.close(dbc2, false);
            if (!err) {
              console.log("Database initialized.");
              console.log("Time elapsed: " + timer.end("dbInit", true) + " (" + timer.end("dbInit") + "ms)");
            }
          });
        });
      });
    },
    function () {
      console.log("Database initialization canceled.");
    }
  );
}


// -----------------------------------------------------------------------------------------------------------------------------------
function dbWipe() {
  utilGetUserConfirm("Type 'WIPE' to wipe the database:", "wipe",
    function () {
      timer.start("dbWipe");
      var dbc = dbi.open(true);
      dbi.execQry(dbc, "TRUNCATE txt, chapter, page, question RESTART IDENTITY CASCADE;", function (err, res) {
        dbi.close(dbc, true, true, null, function () {
          if (!err) {
            console.log("Database wiped.");
            console.log("Time elapsed: " + timer.end("dbWipe", true) + " (" + timer.end("dbWipe") + "ms)");
          }
        });
      });
    },
    function () {
      console.log("Database wipe canceled.");
    }
  );
}


// -----------------------------------------------------------------------------------------------------------------------------------
/**
  * Removes the content of a directory and optionally (doRemDir) the directory itself.
  */
function fsDirRemCont(path, doRemDir) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function (file, index) {
      var currPath = path + "/" + file;
      if (fs.lstatSync(currPath).isDirectory()) fsDirRemCont(currPath, true);
      else fs.unlinkSync(currPath);
    });
    if (doRemDir) fs.rmdirSync(path);
  }
};


// -----------------------------------------------------------------------------------------------------------------------------------
function log(filename, addTimestamp, fields, delimiter) {
  if (fields.length === 0) return;

  if (!fs.existsSync(DIR_LOG)) fs.mkdirSync(DIR_LOG);
  fs.appendFileSync(DIR_LOG + "/" + filename, (addTimestamp ? moment().format() + (delimiter || "\t") : "") + fields.join(delimiter || "\t") + "\n");
}


// -----------------------------------------------------------------------------------------------------------------------------------
function miscGenImg(fname, lineLen, pageLineCnt, doPrintDiagInf, imgW, imgH, imgBgColor, imgFontColor, imgFontFamily, imgFontSize, imgFontWeight, imgCharSpace, imgLineSpace, doCenter, imgTxtOffsetX, imgTxtOffsetY, imgTxt) {
  // (1) Split text into lines of the correct length:
  // (1.1) Process paragraphs:
  var L = [];  // lines
  var l = "";  // current line

  imgTxt.split("\\n").map(
    function (p) {
      l = "";

      // Process words:
      p.split(" ").map(
        function (w) {
          if (l.length + w.length <= lineLen) {
            l += w + " ";
          }
          else {
            l = l.substr(0, l.length - 1);
            L.push(l);
            l = (w.trim().length === 0 ? "" : w + " ");
          }
        }
      );

      // All words processed but a non-empty line may still exist:
      if (l.trim().length > 0) {
        l = l.substr(0, l.length - 1);
        L.push(l);
        l = "";
      }
      else L.push("");
    }
  );
  if (l.trim().length > 0) L.push(l);

  // (1.2) Split lines into pages:
  var P = [];  // pages
  var currPageIdx = 0;
  var currPageLineCnt = 0;
  var wordCnt = 1;

  L.map(
    function (l) {
      if (currPageLineCnt >= pageLineCnt) {
        currPageIdx++;
        currPageLineCnt = 0;
      }
      if (!P[currPageIdx]) P[currPageIdx] = { lines: [] };
      var p = P[currPageIdx];

      if (currPageLineCnt === 0 && l.trim().length === 0) return;  // skip empty leading lines

      currPageLineCnt++;
      wordCnt += l.split(/\s+/).length - 1;

      p.lines.push((doPrintDiagInf ? (currPageLineCnt < 10 ? " " : "") + currPageLineCnt + ": " : "") + l + (doPrintDiagInf ? " [" + l.length + "; " + (wordCnt - l.split(/\s+/).length + 1) + "-" + wordCnt + "]" : ""));

      if (l.trim().length > 0) wordCnt++;
    }
  );

  // (2) Generate images:
  if (!fs.existsSync(DIR_WORK)) fs.mkdirSync(DIR_WORK);

  var i = 1;
  P.map(function (p) {
    var out = sh.exec("convert -antialias -density 288 -size " + (imgW*4) + "x" + (imgH*4) + " xc:white -background \"#" + imgBgColor + "\" -fill \"#" + imgFontColor + "\" -font " + imgFontFamily + " -pointsize " + imgFontSize + " -weight " + imgFontWeight + " -kerning " + imgCharSpace + " -interline-spacing " + imgLineSpace + " -gravity " + (doCenter ? "center" : "None") + " -annotate +" + imgTxtOffsetX + "+" + imgTxtOffsetY + " \"" + p.lines.join("\\n").replace(/"/g, "\\\"") + "\" -resize 25% " + DIR_WORK + "/" + fname + (P.length > 1 ? "-" + (i < 10 ? "0" + i : i) : "") + ".png", { silent: true }).output;
    fs.appendFileSync(DIR_WORK + "/misc-gen-img.log", fname + ":\n" + out + "\n");
    i++;
  });

}


// -----------------------------------------------------------------------------------------------------------------------------------
function pushover(title, body, priority) {
  if (!PUSHOVER_USR_KEY || PUSHOVER_USR_KEY.length === 0 || !PUSHOVER_APP_KEY || PUSHOVER_APP_KEY.length === 0) return;

  if (os.hostname() !== PUSHOVER_HOST) return;

  var p = new push({ user: PUSHOVER_USR_KEY, token: PUSHOVER_APP_KEY });
  p.send({ title: title, message: (!body || body.length === 0 ? null : body), device: PUSHOVER_DEV, sound: PUSHOVER_SND, priority: priority });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function questionAdd(resHttp, chapterId, q, a1, a2, a3, a4, wordA, wordB) {
  var dbc = dbi.open(false);
  dbi.getId(dbc, "SELECT NULL LIMIT 0;", "INSERT INTO question (chapter_id, q, a1, a2, a3, a4, word_a, word_b) VALUES (" + chapterId + "," + dbi.escStr(q) + "," + dbi.escStr(a1) + "," + dbi.escStr(a2) + "," + dbi.escStr(a3) + "," + dbi.escStr(a4) + "," + wordA + "," + wordB + ");", function (questionId) {
    questionGet(resHttp, questionId);
    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function questionGet(resHttp, questionId) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "SELECT * FROM question WHERE id = " + questionId + ";", function (err, result) {
    if (resHttp) {
      if (result.rows.length === 0) {
        serverSend(resHttp, "{ok:false,res:null,msg:\"Question not found.\"}");
      }
      else {
        var q = result.rows[0];
        serverSend(resHttp, "{ok:true,res:{id:" + q.id + ",q:" + utilEscStr(q.q) + ",a1:" + utilEscStr(q.a1) + ",a2:" + utilEscStr(q.a2) + ",a3:" + utilEscStr(q.a3) + ",a4:" + utilEscStr(q.a4) + ",wordA:" + q.word_a + ",wordB:" + q.word_b + "}}");
      }
    }
    else console.log(result.rows.length === 0 ? "null" : q.q + " " + q.a + " " + q.word_a + " " + q.word_b);

    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function questionLst(resHttp, chapterId) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "SELECT q.* FROM question q INNER JOIN chapter c ON q.chapter_id = c.id WHERE c.id = " + chapterId + " ORDER BY q.word_a, q.id;", function (err, result) {
    if (resHttp) {
      var js = "{ok:true,res:[" + result.rows.map(
        function (x) {
          return "{id:" + x.id + ",q:" + utilEscStr(x.q) + ",a1:" + utilEscStr(x.a1) + ",a2:" + utilEscStr(x.a2) + ",a3:" + utilEscStr(x.a3) + ",a4:" + utilEscStr(x.a4) + ",wordA:" + x.word_a + ",wordB:" + x.word_b + "}";
        }
      ).join(",") + "]}";

      serverSend(resHttp, js);
    }
    else {
      var t = tt(result.rows.map(
        function (x) {
          return [ x.id, x.q, x.a1, x.a2, x.a3, x.a4, x.word_a, x.word_b ];
        }
      ));

      console.log(t);
    }

    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function questionRem(resHttp, questionId) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "DELETE FROM question WHERE id = " + questionId + ";", function (err, res) {
    if (resHttp) serverSend(resHttp, "{ok:true}");
    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function questionUpd(resHttp, questionId, q, a1, a2, a3, a4, wordA, wordB) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "UPDATE question SET q = " + dbi.escStr(q) + ", a1 = " + dbi.escStr(a1) + ", a2 = " + dbi.escStr(a2) + ", a3 = " + dbi.escStr(a3) + ", a4 = " + dbi.escStr(a4) + ", word_a = " + wordA + ", word_b = " + wordB + " WHERE id = " + questionId + ";", function (err, res) {
    questionGet(resHttp, questionId);
    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
/**
  * Returns the 'data' to an HTTP client.
  */
function serverSend(res, data, contentType) {
  res.writeHead(200, { "Content-Length": data.length, "Content-Type": (contentType || "text/plain") });
  res.write(data);
  res.end();

  /*
  res.setHeader("Content-Length", data.length);
  res.setHeader("Content-Type", (contentType || "text/plain"));
  res.statusCode = 200;
  res.write();
  res.end(data);
  */
}


// -----------------------------------------------------------------------------------------------------------------------------------
function serverSendFile(res, path, mimeType) {
  fs.readFile(path, function(err, data) {
    if (!err) {
      res.setHeader("Content-Length", data.length);
      res.setHeader("Content-Type", mimeType);
      res.statusCode = 200;
      res.end(data);
    }
    else {
      res.writeHead(500);
      res.end();
    }
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
/**
 * Starts the HTTP server.
 */
function serverStart() {
  http.createServer(function (req, res) {
    // (1) Serve a file:
    var fname    = req.url || "index.html";
    var fpath    = __dirname;
    var fext     = path.extname(fname);
    var mimeType = VALID_FILE_EXT[fext];

    if (mimeType) {  // http://localhost:9999/index.html
      fpath += fname;
      fs.exists(fpath, function(doesExists) {
        if (doesExists) {
          serverSendFile(res, fpath, mimeType);
          serverStart_log(req, "Serving file: ", "ok", fpath);
        }
        else {
          res.writeHead(404);
          res.end();
          serverStart_log(req, "File not found: ", "file not found", fpath);
        }
      });
    }

    // (2) Execute a command:
    else {
      var up = url.parse(req.url);
      var qs = utilGetQS(up.query);

      var cmd = qs["cmd"];

      if (up.pathname.substr(1) == "do" && cmd) {
        switch (cmd) {
          case "chapter-get-content":  // http://localhost:9999/do?cmd=chapter-get-content&chapter-id=1
            var chapterId = parseInt(qs["chapter-id"]);

            if (isNaN(chapterId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            chapterGetContent(res, chapterId);

            serverStart_log(req, "Executing command: ", "ok", cmd + " chapterId:" + chapterId);
            break;

          case "chapter-lst":  // http://localhost:9999/do?cmd=chapter-lst&txt-id=5&do-get-content=0
            var txtId        = parseInt(qs["txt-id"]);
            var doGetContent = qs["do-get-content"] == "1";

            if (isNaN(txtId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            chapterLst(res, txtId, doGetContent);
            serverStart_log(req, "Executing command: ", "ok", cmd + " txtId:" + txtId);
            break;

          case "chapter-upd-content":  // http://localhost:9999/do?cmd=chapter-upd-content&chapter-id=2&content=Very%20interesting%0Amultiline%20content
            var chapterId = parseInt(qs["chapter-id"]);
            var content   = (qs["content"] ? decodeURIComponent(qs["content"]) : null);

            if (isNaN(chapterId) || !content) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            chapterUpdContent(res, chapterId, content);

            serverStart_log(req, "Executing command: ", "ok", cmd + " chapterId:" + chapterId + " contentLength:" + content.length);
            break;

          case "ping":
            serverSend(res, "pong");
            serverStart_log(req, "Executing command: ", "ok", cmd);
            break;

          case "question-add":  // http://localhost:9999/do?cmd=question-add&chapter-id=1&q=Hello there%3F&a=&quot;Hello&quot; indeed!&word-a=20&word-b=73
            var chapterId = parseInt(qs["chapter-id"]);
            var q         = (qs["q"]  ? decodeURIComponent(qs["q"])  : null);
            var a1        = (qs["a1"] ? decodeURIComponent(qs["a1"]) : null);
            var a2        = (qs["a2"] ? decodeURIComponent(qs["a2"]) : null);
            var a3        = (qs["a3"] ? decodeURIComponent(qs["a3"]) : "");
            var a4        = (qs["a4"] ? decodeURIComponent(qs["a4"]) : "");
            var wordA     = parseInt(qs["word-a"]);
            var wordB     = parseInt(qs["word-b"]);

            if (isNaN(chapterId) || !q || q.length === 0 || !a1 || !a2 || (a1.length === 0) || (a2.length === 0) || isNaN(wordA) || isNaN(wordB)) {
              return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");
            }

            questionAdd(res, chapterId, q, a1, a2, a3, a4, wordA, wordB);

            serverStart_log(req, "Executing command: ", "ok", cmd + " chapterId:" + chapterId);
            break;

          case "question-get":  // http://localhost:9999/do?cmd=question-get&question-id=1
            var questionId = parseInt(qs["question-id"]);

            if (isNaN(questionId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            questionGet(res, questionId);

            serverStart_log(req, "Executing command: ", "ok", cmd + " questionId:" + questionId);
            break;

          case "question-lst":  // localhost:10000/do?cmd=question-lst&chapter-id=1
            var chapterId = parseInt(qs["chapter-id"]);

            if (isNaN(chapterId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            questionLst(res, chapterId);

            serverStart_log(req, "Executing command: ", "ok", cmd + " chapterId:" + chapterId);
            break;

          case "question-rem":  // http://localhost:9999/do?cmd=question-rem&question-id=-1
            var questionId = parseInt(qs["question-id"]);

            if (isNaN(questionId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            questionRem(res, questionId);

            serverStart_log(req, "Executing command: ", "ok", cmd + " questionId:" + questionId);
            break;

          case "question-upd":  // http://localhost:9999/do?cmd=question-upd&question-id=2&q=Well well well%3F&a=&quot;Indeed,&quot; good sir!&word-a=98&word-b=99
            var questionId = parseInt(qs["question-id"]);

            if (isNaN(questionId)) return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");

            var q      = (qs["q"] ? decodeURIComponent(qs["q"]) : null);
            var a1     = (qs["a1"] ? decodeURIComponent(qs["a1"]) : null);
            var a2     = (qs["a2"] ? decodeURIComponent(qs["a2"]) : null);
            var a3     = (qs["a3"] ? decodeURIComponent(qs["a3"]) : "");
            var a4     = (qs["a4"] ? decodeURIComponent(qs["a4"]) : "");
            var wordA  = parseInt(qs["word-a"]);
            var wordB  = parseInt(qs["word-b"]);

            if (isNaN(questionId) || !q || q.length === 0 || !a1 || !a2 || (a1.length === 0) || (a2.length === 0) || isNaN(wordA) || isNaN(wordB)) {
              return serverSend(res, "{ok:false,res:null,msg:\"Invalid arguments.\"}");
            }

            questionUpd(res, questionId, q, a1, a2, a3, a4, wordA, wordB);

            serverStart_log(req, "Executing command: ", "ok", cmd + " questionId:" + questionId);
            break;

          case "txt-lst":  // http://localhost:9999/do?cmd=txt-lst
            txtLst(res);
            serverStart_log(req, "Executing command: ", "ok", cmd);
            break;

          default:
            serverSend(res, "{ok:false,res:null,msg:\"Invalid command: " + cmd + "\"}");
            serverStart_log(req, "Invalid command: ", "invalid command", cmd);
        }
      }
      else {
        res.writeHead(404);
        res.end();
        serverStart_log(req, "Invalid request: ", "invalid request", up.pathname.substr(1));
      }
    }
  }).listen(SERVER_PORT);

  console.log("Medusa server listening on port " + SERVER_PORT);
}


// ----^----
function serverStart_log(req, s01, s02, s03) {
  console.log(s01 + s03);
  log("http.log", true, [ s02, req.connection.remoteAddress, s03 ]);
}


// -----------------------------------------------------------------------------------------------------------------------------------
/**
  * pageCntMax=0 indicates no page number restriction
  *
  * Experiment Builder (EB) data source table has the following columns:
  *
  *   Q0: name, chNum, pgNum, aCorrect
  *   Q1: name, type, chNum, pgNum, qNumCh, qNumPg, qId, aCnt, aOrd, aOrdCorrect, qWordA, qWordB
  *
  */
function txtGenStimuli(txtId, lang2, pageSep, lineLen, pageLineCnt, doPrintDiagInf, contentImgCntMax, questionImgCntMax, imgW, imgH, imgBgColor, imgFontColor, imgFontFamily, imgFontSize, imgFontWeight, imgCharSpace, imgLineSpace, imgTxtOffsetX, imgTxtOffsetY, imgChapterSubtxt, lang) {
  timer.start("txtGenStimuli");

  var lang = dbiSync.execQry("SELECT lang FROM txt WHERE id = " + txtId + ";")[0].lang;
  console.log(lang);

  // (1) Prepare the filesystem:
  // (1.1) Directories:
  var dirTxtId    = DIR_STIM + "/" + txtId;
  var dirRoot     = dirTxtId + "/" + (new Date().getTime()) + "--" + imgW + "x" + imgH + "." + pageLineCnt + "x" + lineLen + "." + imgBgColor + "." + imgFontColor + "." + imgFontFamily + "." + imgFontWeight + "." + imgCharSpace + "." + imgLineSpace + "." + imgTxtOffsetX + "." + imgTxtOffsetY;
  var dirTxt      = dirRoot  + "/txt";
  var dirTxtWin   = dirRoot  + "/txt-win";
  var dirImg      = dirRoot  + "/img";
  var dirEB       = dirRoot  + "/eb";
  var dirPegasus  = dirRoot  + "/pegasus";

  if (!fs.existsSync(DIR_STIM)) fs.mkdirSync(DIR_STIM);
  if (!fs.existsSync(dirTxtId)) fs.mkdirSync(dirTxtId);

  fs.mkdirSync(dirRoot   );
  fs.mkdirSync(dirTxt    );
  fs.mkdirSync(dirTxtWin );
  fs.mkdirSync(dirImg    );
  fs.mkdirSync(dirEB);
  fs.mkdirSync(dirPegasus);

  // (1.2) Files:
  var fContent      = dirTxt    + "/content.txt";                  // book content (cut into pages)
  var fContentWin   = dirTxtWin + "/content.txt";                  // book content (cut into pages; Windows end lines)
  var fQuestions    = dirTxt    + "/questions.txt";                // questions
  var fQuestionsWin = dirTxtWin + "/questions.txt";                // questions (Windows end lines)
  var fEBTblQ0      = dirTxt    + "/eb-tbl-q0.txt";                // experiment builder table (no questions)
  var fEBTblQ0Win   = dirTxtWin + "/eb-tbl-q0.txt";                // experiment builder table (no questions; Windows end lines)
  var fEBTblQ1AP    = dirTxt    + "/eb-tbl-q1-after-page.txt";     // experiment builder table (questions after page)
  var fEBTblQ1APWin = dirTxtWin + "/eb-tbl-q1-after-page.txt";     // experiment builder table (questions after page; Windows end lines)
  var fEBTblQ1AC    = dirTxt    + "/eb-tbl-q1-after-chapter.txt";  // experiment builder table (questions after chapter)
  var fEBTblQ1ACWin = dirTxtWin + "/eb-tbl-q1-after-chapter.txt";  // experiment builder table (questions after chapter; Windows end lines)
  var fEBTblInf     = dirTxt    + "/eb-tbl-inf.txt";               // experiment builder table info
  var fEBTblInfWin  = dirTxtWin + "/eb-tbl-inf.txt";               // experiment builder table info (Windows end lines)
  var fImgLog       = dirImg    + "/img.log";                      // image creation log

  fs.appendFileSync(fQuestions,    [ "text-id", "chapter-id", "question-id", "chapter-number", "page-number", "page-name", "question-chapter-num", "question-page-num", "question-name", "question", "answer-1-correct", "answer-2", "answer-3", "answer-4", "answers-order", "answers-order-correct", "word-a", "word-b" ].join("\t") +   "\n");
  fs.appendFileSync(fQuestionsWin, [ "text-id", "chapter-id", "question-id", "chapter-number", "page-number", "page-name", "question-chapter-num", "question-page-num", "question-name", "question", "answer-1-correct", "answer-2", "answer-3", "answer-4", "answers-order", "answers-order-correct", "word-a", "word-b" ].join("\t") + "\r\n");

  fs.appendFileSync(fEBTblInf,    [ "name", "type", "chapter-num", "page-num", "question-num-chapter", "question-num-page", "question-id", "question-answer-cnt", "question-answer-ord", "question-answer-corr", "question-word-a", "question-word-b" ].join("\t") +   "\n");
  fs.appendFileSync(fEBTblInfWin, [ "name", "type", "chapter-num", "page-num", "question-num-chapter", "question-num-page", "question-id", "question-answer-cnt", "question-answer-ord", "question-answer-corr", "question-word-a", "question-word-b" ].join("\t") + "\r\n");

  // (2) Generate text, images, and the EB table:
  var contentImgCntTot  = 0;
  var questionImgCntTot = 0;
  var ebTbl = [];

  var resC = dbiSync.execQry("SELECT c.id, c.num, c.content FROM chapter c INNER JOIN txt t ON c.txt_id = t.id WHERE t.id = " + txtId + " ORDER BY c.num;");
  resC.map(function (c) {
    timer.start("txtGenStimuli_chapter");

    // (2.1) Read the chapter's content and split it into lines of the correct length:
    var L = [];  // lines
    var l = "";  // current line

    // Process paragraphs:
    c.content.split("\n").map(
      function (p) {
        l = "";

        // Process words:
        p.split(" ").map(
          function (w) {
            if (l.length + w.length <= lineLen) {
              l += w + " ";
            }
            else {
              l = l.substr(0, l.length - 1);
              L.push(l);
              l = (w.trim().length === 0 ? "" : w + " ");
            }
          }
        );

        // All words processed but a non-empty line may still exist:
        if (l.trim().length > 0) {
          l = l.substr(0, l.length - 1);
          L.push(l);
          l = "";
        }
        else L.push("");
      }
    );
    if (l.trim().length > 0) L.push(l);

    // (2.2) Split lines into pages:
    var P = [];  // pages
    var currPageIdx = 0;
    var currPageLineCnt = 0;
    var wordCnt = 1;  // keeps track of the number of words processed so far; used for wordA and wordB for pages

    L.map(
      function (l) {
        if (currPageLineCnt >= pageLineCnt) {
          currPageIdx++;
          currPageLineCnt = 0;
        }
        if (!P[currPageIdx]) P[currPageIdx] = { lines: [], wordA: wordCnt, wordB: wordCnt };
        var p = P[currPageIdx];

        if (currPageLineCnt === 0 && l.trim().length === 0) return;  // skip empty leading lines

        currPageLineCnt++;
        wordCnt += l.split(/\s+/).length - 1;

        p.lines.push((doPrintDiagInf ? (currPageLineCnt < 10 ? " " : "") + currPageLineCnt + ": " : "") + l + (doPrintDiagInf ? " [" + l.length + "; " + (wordCnt - l.split(/\s+/).length + 1) + "-" + wordCnt + "]" : ""));
        p.wordB = wordCnt;

        if (l.trim().length > 0) wordCnt++;
      }
    );

    // (2.3) Generate chapter image:
    var chapterName = "c" + (c.num < 10 ? "0" : "") + c.num;
    if (contentImgCntMax === -1 || contentImgCntTot < contentImgCntMax) {
      var out = sh.exec("convert -antialias -density 288 -size " + (imgW*4) + "x" + (imgH*4) + " xc:white -background \"#" + imgBgColor + "\" -fill \"#" + imgFontColor + "\" -font " + imgFontFamily + " -pointsize " + imgFontSize + " -weight " + imgFontWeight + " -kerning " + imgCharSpace + " -interline-spacing " + imgLineSpace + " -gravity None -annotate +" + imgTxtOffsetX + "+" + imgTxtOffsetY + " \"" + IMG_TXT_CHAPTER[lang] + " " + c.num + "\\n\\n" + imgChapterSubtxt + "\" -resize 25% " + dirImg + "/" + chapterName + ".png", { silent: true }).output;
      fs.appendFileSync(fImgLog, chapterName + ":\n" + out + "\n");
      contentImgCntTot++;

      // (2.3.1) Create Pegasus stimulus subdirectory:
      var dirPegasusChapter = dirPegasus + "/" + chapterName;
      fs.mkdirSync(dirPegasusChapter);

      // (2.3.2) Copy to EB and Pegasus:
      fs.createReadStream(dirImg + "/" + chapterName + ".png").pipe(fs.createWriteStream(dirEB + "/" + imgW + "x" + imgH + "-" + lang + "-" + chapterName + ".png"));
      fs.createReadStream(dirImg + "/" + chapterName + ".png").pipe(fs.createWriteStream(dirPegasusChapter + "/" + chapterName + ".png"));
    }

    // (2.4) Populate EB table:
    ebTbl.push({ name: chapterName, txtId: txtId, chapterNum: c.num, chapterName: chapterName, pageNum: 0, pageName: "", questionId: 0, questionNumChapter: 0, questionNumPage: 0, questionName: "", questionACnt: "0", questionAOrd: ".", questionACorr: 0, wordA: 0, wordB: 0 });

    // (2.4) Generate text and images:
    var currPageNum = 1;
    P.map(function (p) {
      p.num  = currPageNum;
      p.name = chapterName + "p" + (currPageNum < 10 ? "0" : "") + currPageNum + (doPrintDiagInf ? "  [words " + p.wordA + " to " + p.wordB + "]" : "");

      // (2.4.1) Create Pegasus stimulus subdirectory:
      var dirPegasusPage = dirPegasus + "/" + p.name;
      fs.mkdirSync(dirPegasusPage);

      // (2.4.2) Text:
      fs.appendFileSync(fContent,    pageSep + " " + p.name +   "\n" + p.lines.join("\n") +   "\n");
      fs.appendFileSync(fContentWin, pageSep + " " + p.name + "\r\n" + p.lines.join("\n") + "\r\n");

      fs.appendFileSync(dirPegasusPage + "/" + p.name + ".txt", p.lines.join("\n"));

      // (2.4.3) Image:
      if (contentImgCntMax === -1 || contentImgCntTot < contentImgCntMax) {
        var out = sh.exec("convert -antialias -density 288 -size " + (imgW*4) + "x" + (imgH*4) + " xc:white -background \"#" + imgBgColor + "\" -fill \"#" + imgFontColor + "\" -font " + imgFontFamily + " -pointsize " + imgFontSize + " -weight " + imgFontWeight + " -kerning " + imgCharSpace + " -interline-spacing " + imgLineSpace + " -gravity None -annotate +" + imgTxtOffsetX + "+" + imgTxtOffsetY + " \"" + p.lines.join("\\n").replace(/"/g, "\\\"") + "\" -resize 25% " + dirImg + "/" + p.name + ".png", { silent: true }).output;
        fs.appendFileSync(fImgLog, p.name + ":\n" + out + "\n");
        contentImgCntTot++;

        // Copy to EB and Pegasus:
        fs.createReadStream(dirImg + "/" + p.name + ".png").pipe(fs.createWriteStream(dirEB + "/" + imgW + "x" + imgH + "-" + lang + "-" + p.name + ".png"));
        fs.createReadStream(dirImg + "/" + p.name + ".png").pipe(fs.createWriteStream(dirPegasusPage + "/" + p.name + ".png"));
      }

      // (2.4.4) Populate the EB table:
      ebTbl.push({ name: p.name, txtId: txtId, chapterNum: c.num, chapterName: chapterName, pageNum: p.num, pageName: p.name, questionId: 0, questionNumChapter: 0, questionNumPage: 0, questionName: "", questionACnt: "0", questionAOrd: ".", questionACorr: 0, wordA: 0, wordB: 0 });

      currPageNum++;
    });

    // (2.5) Generate questions report and images:
    // (2.5.1) Process questions:
    var Q = [];  // questions (needed to sort questions before adding them to the report file)

    var resQ = dbiSync.execQry("SELECT q.* FROM question q INNER JOIN chapter c ON q.chapter_id = c.id WHERE c.id = " + c.id + " ORDER BY q.word_a, q.word_b, q.id;");
    resQ.map(function (q) {
      for (var i = 0; i < P.length; i++) {
        var p = P[i];

        if ((q.word_a >= p.wordA && q.word_b <= p.wordB) || (q.word_a < p.wordA && q.word_b <= p.wordB) || i === P.length - 1) {
          // Shuffle answers:
          var aOrd = [];  // order of answers
          if (q.a4.length > 0) aOrd = [1,2,3,4];
          else {
            if (q.a3.length > 0) aOrd = [1,2,3];
            else aOrd = [1,2];
          }
          utilArrShuffle(aOrd);
          var aOrdCorr = 0;
          while (aOrd[aOrdCorr] !== 1) aOrdCorr++;  // find the index of answer #1 (which is always the correct one)
          aOrdCorr++;  // convert the index to the order number

          // Associate with page:
          q.pageNum  = i + 1;
          q.pageName = "c" + (c.num < 10 ? "0" : "") + c.num + "p" + ((i+1) < 10 ? "0" : "") + (i+1);

          // Add question:
          Q.push({ name: q.name, txtId: txtId, chapterId: c.id, chapterNum: c.num, pageNum: q.pageNum, pageName: q.pageName, id: q.id, questionNumChapter: -1, questionNumPage: -1, q: q.q.trim(), a1: q.a1.trim(), a2: q.a2.trim(), a3: q.a3.trim(), a4: q.a4.trim(), aOrd: aOrd, aOrdCorr: aOrdCorr, wordA: q.word_a, wordB: q.word_b });

          break;
        }
      }
    });

    // (2.5.2) Sort questions:
    Q.sort(
      function (a,b) {
        if (a.pageNum - b.pageNum !== 0) return a.pageNum - b.pageNum;
        if (a.wordA   - b.wordA   !== 0) return a.wordA   - b.wordA  ;
        if (a.wordB   - b.wordB   !== 0) return a.wordB   - b.wordB  ;
                                         return a.id      - b.id     ;
    });

    // (2.5.3) Generate report and images:
    var prevPageName    = "";
    var questionCnt     = 0;
    var questionCntPage = 0;

    Q.map(function (q) {
      if (q.pageName === prevPageName) questionCntPage++;
      else {
        prevPageName    = q.pageName;
        questionCntPage = 0;
      }

      q.name               = q.pageName + "q" + (questionCntPage < 9 ? "0" : "") + (questionCntPage + 1);
      q.numChapter         = questionCnt + 1;
      q.numPage            = questionCntPage + 1;

      questionCnt++;

      // Report:
      fs.appendFileSync(fQuestions,    [ q.txtId, q.chapterId, q.id, q.chapterNum, q.pageNum, q.pageName, q.numChapter, q.numPage, q.name, q.q, q.a1, q.a2, q.a3, q.a4, q.aOrd.join(","), q.aOrdCorr, q.wordA, q.wordB ].join("\t") +   "\n");
      fs.appendFileSync(fQuestionsWin, [ q.txtId, q.chapterId, q.id, q.chapterNum, q.pageNum, q.pageName, q.numChapter, q.numPage, q.name, q.q, q.a1, q.a2, q.a3, q.a4, q.aOrd.join(","), q.aOrdCorr, q.wordA, q.wordB ].join("\t") + "\r\n");

      // EB table:
      ebTbl.push({ name: q.name, txtId: txtId, chapterNum: c.num, chapterName: chapterName, pageNum: q.pageNum, pageName: q.pageName, questionId: q.id, questionNumChapter: q.numChapter, questionNumPage: q.numPage, questionName: q.name, questionACnt: q.aOrd.length, questionAOrd: q.aOrd.join(""), questionACorr: q.aOrdCorr, wordA: q.wordA, wordB: q.wordB });

      // Image:
      if (questionImgCntMax === -1 || questionImgCntTot < questionImgCntMax) {
        // Split question into lines:
        var QL = [];  // question lines
        var ql = "";
        q.q.split(" ").map(
          function (w) {
            if (ql.length + w.length <= lineLen) {
              ql += w + " ";
            }
            else {
              ql = ql.substr(0, ql.length - 1);
              QL.push(ql);
              ql = (w.trim().length === 0 ? "" : w + " ");
            }
          }
        );

        if (ql.trim().length > 0) {
          ql = ql.substr(0, ql.length - 1);
          QL.push(ql);
          ql = "";
        }
        else QL.push("");

        // Create the complete image text:
        var txt =
          QL.join("\n") + "\n\n\n" +
          "A) " + IMG_TXT_AA[lang] + "\n" +
          "B) " + IMG_TXT_AB[lang] + "\n" +
          "C) " + IMG_TXT_AC[lang] + "\n\n" +
          "1) " + q["a" + q.aOrd[0]] + "\n" +
          "2) " + q["a" + q.aOrd[1]] + "\n" +
          (q.aOrd.length >= 3 ? "3) " + q["a" + q.aOrd[2]] + "\n" : "") +
          (q.aOrd.length >= 4 ? "4) " + q["a" + q.aOrd[3]] + "\n" : "");
          /*
          QL.join("\\n") + "\\n\\n\\n" +
          "A) " + IMG_TXT_AA[lang] + "\\n" +
          "B) " + IMG_TXT_AB[lang] + "\\n" +
          "C) " + IMG_TXT_AC[lang] + "\\n\\n" +
          "1) " + q["a" + q.aOrd[0]] + "\\n" +
          "2) " + q["a" + q.aOrd[1]] + "\\n" +
          (q.aOrd.length >= 3 ? "3) " + q["a" + q.aOrd[2]] + "\\n" : "") +
          (q.aOrd.length >= 4 ? "4) " + q["a" + q.aOrd[3]] + "\\n" : "");
          */

        // Create Pegasus stimulus subdirectory:
        var dirPegasusQuestion = dirPegasus + "/" + q.name;
        fs.mkdirSync(dirPegasusQuestion);

        // Text:
        fs.appendFileSync(dirPegasusQuestion + "/" + q.name + ".txt", txt);

        // Image:
        var out = sh.exec("convert -antialias -density 288 -size " + (imgW*4) + "x" + (imgH*4) + " xc:white -background \"#" + imgBgColor + "\" -fill \"#" + imgFontColor + "\" -font " + imgFontFamily + " -pointsize " + imgFontSize + " -weight " + imgFontWeight + " -kerning " + imgCharSpace + " -interline-spacing " + imgLineSpace + " -gravity None -annotate +" + imgTxtOffsetX + "+" + imgTxtOffsetY + " \"" + txt.replace(/"/g, "\\\"").replace(/\n/g, "\\n") + "\" -resize 25% " + dirImg + "/" + q.name + ".png", { silent: true }).output;
        fs.appendFileSync(fImgLog, q.name + ":\n" + out + "\n");

        fs.createReadStream(dirImg + "/" + q.name + ".png").pipe(fs.createWriteStream(dirEB + "/" + imgW + "x" + imgH + "-" + lang + "-" + q.name + ".png"));
        fs.createReadStream(dirImg + "/" + q.name + ".png").pipe(fs.createWriteStream(dirPegasusQuestion + "/" + q.name + ".png"));

        questionImgCntTot++;
      }
    });
  });

  // (2.6) Sort and persist EB table:
  // (2.6.1) Questions right after page:
  // Sort:
  ebTbl.sort(function (a,b) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return  1;
                         return  0;
  });
  // Persist:
  fs.appendFileSync(fEBTblQ1AP,    [ "title", "t", 0, 0, 0, 0, 0, 0, ".", 0, 0, 0 ].join("\t")   + "\n");
  fs.appendFileSync(fEBTblQ1APWin, [ "title", "t", 0, 0, 0, 0, 0, 0, ".", 0, 0, 0 ].join("\t") + "\r\n");
  ebTbl.map(function (x) {
    fs.appendFileSync(fEBTblQ1AP,    [ x.name, (x.questionId === 0 ? "t" : "q"), x.chapterNum, x.pageNum, x.questionNumChapter, x.questionNumPage, x.questionId, x.questionACnt, x.questionAOrd, x.questionACorr, x.wordA, x.wordB ].join("\t")   + "\n");
    fs.appendFileSync(fEBTblQ1APWin, [ x.name, (x.questionId === 0 ? "t" : "q"), x.chapterNum, x.pageNum, x.questionNumChapter, x.questionNumPage, x.questionId, x.questionACnt, x.questionAOrd, x.questionACorr, x.wordA, x.wordB ].join("\t") + "\r\n");
  });

  // (2.6.2) Questions after entire chapter:
  // Sort:
  ebTbl.sort(function (a,b) {
    if (a.chapterNum < b.chapterNum) return -1;
    if (a.chapterNum > b.chapterNum) return  1;

    if ((a.questionId === 0 && b.questionId === 0) || (a.questionId > 0 && b.questionId > 0)) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return  1;
                           return  0;
    }
    else {
      if (a.questionId === 0) return -1;
      if (b.questionId === 0) return  1;
                              return  0;
    }
  });
  // Persist:
  fs.appendFileSync(fEBTblQ1AC,    [ "title", "t", 0, 0, 0, 0, 0, 0, ".", 0, 0, 0 ].join("\t")   + "\n");
  fs.appendFileSync(fEBTblQ1ACWin, [ "title", "t", 0, 0, 0, 0, 0, 0, ".", 0, 0, 0 ].join("\t") + "\r\n");
  ebTbl.map(function (x) {
    fs.appendFileSync(fEBTblQ1AC,    [ x.name, (x.questionId === 0 ? "t" : "q"), x.chapterNum, x.pageNum, x.questionNumChapter, x.questionNumPage, x.questionId, x.questionACnt, x.questionAOrd, x.questionACorr, x.wordA, x.wordB ].join("\t") +   "\n");
    fs.appendFileSync(fEBTblQ1ACWin, [ x.name, (x.questionId === 0 ? "t" : "q"), x.chapterNum, x.pageNum, x.questionNumChapter, x.questionNumPage, x.questionId, x.questionACnt, x.questionAOrd, x.questionACorr, x.wordA, x.wordB ].join("\t") + "\r\n");
  });

  // (2.6.3) No questions:
  // Persist (sorted already, just skip questions):
  fs.appendFileSync(fEBTblQ0,    [ "title", "t", 0, 0, 0, 0 ].join("\t")   + "\n");
  fs.appendFileSync(fEBTblQ0Win, [ "title", "t", 0, 0, 0, 0 ].join("\t") + "\r\n");
  ebTbl.map(function (x) {
    if (x.questionId !== 0) return;  // skip questions
    fs.appendFileSync(fEBTblQ0,    [ x.name, "t", x.chapterNum, x.pageNum, 0, 0 ].join("\t") +   "\n");
    fs.appendFileSync(fEBTblQ0Win, [ x.name, "t", x.chapterNum, x.pageNum, 0, 0 ].join("\t") + "\r\n");
  });

  var t = timer.end("txtGenStimuli_chapter");
  process.stdout.write("Done (" + timer.msToTime(t) + "; " + t + "ms)\n");
}


// -----------------------------------------------------------------------------------------------------------------------------------
function txtLst(resHttp) {
  var dbc = dbi.open(false);
  dbi.execQry(dbc, "SELECT * FROM txt WHERE is_visible = true ORDER BY lang, title_en;", function (err, result) {
    if (resHttp) {
      var js = "{ok:true,res:[" + result.rows.map(
        function (x) {
          return "{id:" + x.id + ",lang:" + utilEscStr(x.lang) + ",title:" + utilEscStr(x.title) + ",titleEn:" + utilEscStr(x.title_en) + ",author:" + utilEscStr(x.author) + ",year:" + x.year + "}";
        }
      ).join(",") + "]}";

      serverSend(resHttp, js);
    }
    else {
      var t = tt(result.rows.map(
        function (x) {
          return [ x.id, x.lang, x.title, x.title_en, x.author, x.year ];
        }
      ));

      console.log(t);
    }

    dbi.close(dbc, false);
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
function utilArrShuffle(A) {
  var i = A.length;

  // While there remain elements to shuffle...
  while (i !== 0) {
    // Pick a remaining element...
    var iRnd = Math.floor(Math.random() * i);
    i--;

    // And swap it with the current element.
    var a = A[i];
    A[i] = A[iRnd];
    A[iRnd] = a;
  }

  return A;
}


// -----------------------------------------------------------------------------------------------------------------------------------
function utilGetQS(s) {
  if (!s || s.length === 0) return {};

  var qs = {};

  var A = s.split("&");
  for (var i=0, ni=A.length; i < ni; i++) {
    var B = A[i].split("=");
    qs[B[0]] = B[1];
  }

  return qs;
}


// -----------------------------------------------------------------------------------------------------------------------------------
function utilGetUserConfirm(prompt, confirmStr, fnY, fnN, doAutoConfirm) {
  if (doAutoConfirm) return fnY();

  var rl = readline.createInterface({ input: process.stdin, output: process.stdout });
  rl.question(prompt + " ", function(answer) {
    if (answer.toLowerCase() === confirmStr) fnY();
    else fnN();
    rl.close();
  });
}


// -----------------------------------------------------------------------------------------------------------------------------------
//function utilEscStr(s) { return "decodeURIComponent(\"" + s.replace(/"/g, "\\\"") + "\")"; }

function utilEscStr(s) { return "decodeURIComponent(\"" + encodeURIComponent(s) + "\")"; }


// -----------------------------------------------------------------------------------------------------------------------------------
function main() {
  var cmd = process.argv[2];

  switch (cmd) {
    case "db-init":
      dbInit();
      break;

    case "db-wipe":
      dbWipe();
      break;

    case "misc-gen-img":  // node do.js misc-gen-img test 105 25 0 1680 1050 eeeeee 000000 Courier 24 - 1 65 180 250 "blah blah\nblah"
      if (process.argv.length <= 19) return console.log("Usage: node " + selfName + " " + cmd + " <filename> <line-length> <page-line-count> <img-width> <img-height> <img-bg-color> <img-font-color> <img-font-family> <img-font-size> <img-font-weight> <img-char-spacing> <img-line-spacing> <do-center> <img-txt-offset-x> <img-txt-offset-y> <text>");

      var fname           = process.argv[3];
      var lineLen         = parseInt(process.argv[4]);
      var pageLineCnt     = parseInt(process.argv[5]);
      var doPrintDiagInf  = !!parseInt(process.argv[6]);
      var imgW            = parseInt(process.argv[7]);
      var imgH            = parseInt(process.argv[8]);
      var imgBgColor      = process.argv[9];
      var imgFontColor    = process.argv[10];
      var imgFontFamily   = process.argv[11];
      var imgFontSize     = parseInt(process.argv[12]);
      var imgFontWeight   = (process.argv[13] === "-" ? 300 : parseInt(process.argv[13]));
      var imgCharSpace    = parseInt(process.argv[14]);
      var imgLineSpace    = parseInt(process.argv[15]);
      var imgDoCenter     = parseInt(process.argv[16]);
      var imgTxtOffsetX   = parseInt(process.argv[17]);
      var imgTxtOffsetY   = parseInt(process.argv[18]);
      var imgTxt         = process.argv[19];

      if (fname.length === 0 || isNaN(lineLen) || isNaN(pageLineCnt) || isNaN(imgW) || isNaN(imgH) || isNaN(imgFontSize) || isNaN(imgFontWeight) || isNaN(imgCharSpace) || isNaN(imgLineSpace) || isNaN(imgDoCenter) || isNaN(imgTxtOffsetX) || isNaN(imgTxtOffsetY)) return console.log("Invalid arguments");

      imgDoCenter = (imgDoCenter === 1);

      miscGenImg(fname, lineLen, pageLineCnt, doPrintDiagInf, imgW, imgH, imgBgColor, imgFontColor, imgFontFamily, imgFontSize, imgFontWeight, imgCharSpace, imgLineSpace, imgDoCenter, imgTxtOffsetX, imgTxtOffsetY, imgTxt);
      break;

    case "ping":
      console.log("pong");
      break;

    case "question-lst":
      var chapterId = parseInt(process.argv[3]);

      if (isNaN(chapterId)) return console.log("Invalid arguments");

      console.log(questionLst(null, chapterId));
      break;

    case "server-start":
      serverStart();
      break;

    case "txt-add":  // psql -U postgres -d medusa -a -f data/sense-and-sensibility-en.sql
      console.log("Not implemented yet.");
      break;

    case "txt-gen-stimuli":  // node do.js txt-gen-stimuli 8 === 105 25 0 1 1 1600 1200 eeeeee 000000 Courier 24 - 1 65 180 260 "" en
      if (process.argv.length <= 23) return console.log("Usage: node " + selfName + " " + cmd + " <text-id> <lang> <page-separator> <line-length> <page-line-count> <print-diag-inf?> <content-img-cnt-max> <question-img-cnt-max> <img-width> <img-height> <img-bg-color> <img-font-color> <img-font-family> <img-font-size> <img-font-weight> <img-char-spacing> <img-line-spacing> <img-txt-offset-x> <img-txt-offset-y> <chapter-subtext> <lang>");

      var txtId             = parseInt(process.argv[3]);
      var lang              = process.argv[4];
      var pageSep           = process.argv[5];
      var lineLen           = parseInt(process.argv[6]);
      var pageLineCnt       = parseInt(process.argv[7]);
      var doPrintDiagInf    = !!parseInt(process.argv[8]);
      var contentImgCntMax  = parseInt(process.argv[9]);
      var questionImgCntMax = parseInt(process.argv[10]);
      var imgW              = parseInt(process.argv[11]);
      var imgH              = parseInt(process.argv[12]);
      var imgBgColor        = process.argv[13];
      var imgFontColor      = process.argv[14];
      var imgFontFamily     = process.argv[15];
      var imgFontSize       = parseInt(process.argv[16]);
      var imgFontWeight     = (process.argv[17] === "-" ? 300 : parseInt(process.argv[17]));
      var imgCharSpace      = parseInt(process.argv[18]);
      var imgLineSpace      = parseInt(process.argv[19]);
      var imgTxtOffsetX     = parseInt(process.argv[20]);
      var imgTxtOffsetY     = parseInt(process.argv[21]);
      var imgChapterSubtxt  = process.argv[22];

      if (isNaN(txtId) || isNaN(lineLen) || isNaN(pageLineCnt) || isNaN(contentImgCntMax) || isNaN(questionImgCntMax) || isNaN(imgW) || isNaN(imgH) || isNaN(imgFontSize) || isNaN(imgFontWeight) || isNaN(imgCharSpace) || isNaN(imgLineSpace) || isNaN(imtTxtOffsetX) || isNaN(imtTxtOffsetY)) return console.log("Invalid arguments");

      txtGenStimuli(txtId, lang, pageSep, lineLen, pageLineCnt, doPrintDiagInf, contentImgCntMax, questionImgCntMax, imgW, imgH, imgBgColor, imgFontColor, imgFontFamily, imgFontSize, imgFontWeight, imgCharSpace, imgLineSpace, imgTxtOffsetX, imgTxtOffsetY, imgChapterSubtxt, lang);
      break;

    case "txt-lst":
      txtLst(null);
      break;

    case "txt-rem":
      console.log("Not implemented yet.");
      break;

    default:
      console.log("Usage: node " + selfName + " <command>")
      console.log("Available commands: db-init, question-lst, misc-gen-img, ping, server-start, txt-add, txt-gen-stimuli, txt-lst, txt-rem");
  }
}


// -----------------------------------------------------------------------------------------------------------------------------------
main();
