# Medusa: Text Corpora Annotation and Stimuli Generation
A Web application and a command-line utility for annotating large corpora of text and generating stimulus images to be used in psycholinguistics and beyond.


## Abstract
Many tasks benefit from human knowledge and experience, a situation unlikely to change any time soon in spite of the recent advances in machine learning.  One example of a task unlikely to be amenable to automation is text annotation.  That task requires not only a good understanding of the text being read but also attention to the smallest details.  Moreover, text annotation has an underlying goal, e.g., identification of information that satisfies certain search criteria which may be hard to define precisely and optimized for by a computer algorithm.  One particular type of text that is often annotated (and processed in other ways) is a text corpus.

_Text corpora_ are large volumes of unstructured text such as books.  They are often used in psycholinguistics because they permit reading to be studied in an ecologically valid setting where entire chapters of even entire books are digested by subjects within one experiment.

In this repository, I present a Web application to annotate text corpora and a command-line utility that allows generation of images of the paginated text and of the annotations themselves.  Because of the focus on reading research, this repository is of special interest to psycholinguists, but it addresses general text annotation needs as well.


## Contents
- [Features](#features)
- [Details](#details)
  * [Annotation](#annotation)
  * [Capabilities](#capabilities)
  * [Data Collection and Data Analysis Software](#data-collection-and-data-analysis-software)
  * [Images](#images)
  * [Web Client](#web-client)
  * [Command-Line Utility: Sample Output](#command-line-utility-sample-output)
    * [Pagination of an Entire Text Corpus](#pagination-of-an-entire-text-corpus)
    * [Annotations](#annotations)
    * [Arbitrary Text](#arbitrary-text)
  * [Command-Line Utility: Stimulus Schedule](#command-line-utility-stimulus-schedule)
  * [Pushover Notifications](#pushover-notifications)
  * [Adapting to New Use Scenarios](#adapting-to-new-use-scenarios)
  * [Concurrent Users](#concurrent-users)
  * [Security](#security)
  * [Multilingual Support](#multilingual-support)
  * [The Name](#the-name)
- [Dependencies](#dependencies)
- [Data Structures](#data-structures)
- [Setup](#setup)
  * [tl;dr](#tldr)
  * [Node.js](#nodejs)
  * [Database Server and The Database](#database-server-and-the-database)
  * [Web Server](#web-server)
  * [Adding and Removing Text Corpora](#adding-and-removing-text-corpora)
  * [ImageMagik](#imagemagik)
- [Command Reference](#command-reference)
- [Acknowledgments](#acknowledgments)
- [Citing](#citing)
- [License](#license)


## Features
- Annotate passages of text with questions related to those passages, with each annotation having:
  * The start and the end word indices
  * Question asking about that passage
  * One correct answer
  * One, two, or three incorrect answers (distractors)
- Generate images of text pages, annotations, and arbitrary text (e.g., experiment instructions), with each image parameterized by:
  * Image dimensions
  * Background color
  * Font (family, weight, size, and color)
  * Character spacing
  * Line spacing
  * Number of lines per page
  * Number of characters per line
- Annotations are optional if the only purpose is to divide text into pages and generate images
- Generate a text file with the text divided into pages exactly as it appears on the generated images (for easy integration with software like [Pegasus](https://gitlab.com/ubiq-x/pegasus))
- Multilingual support


## Details
### Annotation
Annotation is a general term and can mean a multitude of things depending on context.  With respect to the original research for which this software was developed, the goal was to create questions that would later be asked to participants as they read the text on a computer screen.  Those questions needed to be very specific in that the answers to them should be contained within a short passage of text (from a few words to a few sentences), and moreover could not have been known or easily deduced from prior text.  For example, if a venue or a person was being described for the very first time, it was be acceptable to ask about details of that venue or person.  The assumption behind such localized questions was that if crafted with proper care they should reveal if the reader was paying attention to the text while reading the short passage containing the answer.  More specifically, the main goal of the original research was to make inferences about normal and mindless reading based on subjects' answers.  _Mindless reading_ occurs when a person's eyes continue moving across a page of text but little or nothing is understood because the person is thinking about something else altogether (often without being aware of that).

### Capabilities
The Web application presented here allows the user to highlight a passage of text, write the question that would be asked about it, provide the correct answer as well as one, two, or three incorrect answers (distractors).  The application also allows editing any previously added questions as well as editing the text of the corpus itself in case errors are discovered.  All that information is stored in a relational database which can later be queried by the command-line utility in order to paginate the corpus and render images of text pages as well as textual reports that can then used by data collection and data analysis software.

### Data Collection and Data Analysis Software
Medusa can be used with any data collection software because its output consists of plain text and images.  However, because the original research involved recording the subjects' eye movements with the [SR Research](https://www.sr-research.com) EyeLink 1000 eye tracker, the [Experiment Builder](https://www.sr-research.com/experiment-builder) (also made by SR Research) was a natural choice.  Similarly, any data analysis package can be used with the output from this software.  However, Medusa integrates with [Pegasus](https://gitlab.com/ubiq-x/pegasus) by generating a special directory that can easily be imported into the latter.

The experiment I have developed for the purpose of the original research and which uses the output of Medusa is fairly large and contains some custom Python code  (Medusa-specific for that matter).  Moreover, that experiment on its own may be useful for those interested in studying reading and mind-wandering during reading (i.e., _mindless reading_).  For those reasons, I devote a separate repository to that [Experiment Builder experiment](https://gitlab.com/ubiq-x/mindless-read-biling).

### Images
In experimental settings, images of text pages are often preferred over plain text  because there is no ambiguity with respect to how they are going to be rendered on screen.  That consideration is especially important in studies investigating multilingual corpora (which may contain diacritics, require special fonts, etc.) and which are run across multiple research sites possibly having different experimental equipment (e.g., two labs can have displays with different resolutions).  For that reason, Medusa can be used for the sole purpose of text corpus pagination and image rendition (i.e., annotating text is a feature which does not need to be used).  Moreover, apart from images of paginated text corpora, images of arbitrary text can be generated as well which is useful for research protocols, experiment instructions, on-screen probes, etc.  The pagination process as well as the properties of resulting images are heavily parameterized (e.g., the font and its size can be specified) to enable customization and accommodate multiple usage scenarios.  An example of how this customization may be useful is an experiment that investigates differences in subjects' response when the same text is displayed in several different ways (e.g., comparing monospace and proportional fonts).

### Web Client
The Web app has a simple, easy to use, and self-explanatory user interface.  Figures 1 and 2 show Chapter 1 of Jane Austin's _Sense and Sensibility_ novel annotated with several questions.  Figure 1 shows the question authoring mode (which is the default mode) while Figure 2 shows the question editing mode where an existing question is being modified.  Note the different color schemes used to clearly distinguish between these two modes.

![Web client 01](media/medusa-01.png)
<p align="center"><b>Figure 1.</b> Question authoring mode.</p>

![Web client 02](media/medusa-02.png)
<p align="center"><b>Figure 2.</b> Question editing mode.</p>

The Web client remembers its own state.  That is, the annotator can refresh the Web page or close it entirely but when they load it again they will find themselves exactly where they left off.  This releases them from performing annoyingly repetitive navigation and thus improves their experience.  More importantly however it also prevents human errors; user selects their work context once and unless they change it explicitly it will stay the same.

As is clear from the two above screenshots, there is no notion of text pagination at the annotation authoring stage.  That aspect becomes relevant only when images of text pages are generated, which we turn to next.

### Command-Line Utility: Sample Output
The command-line utility with a succinct name of `do.js` presented here is responsible for paginating a text corpus and rendering images of text pages and a number of plain-text and tabular reports.  The utility is invoked in the following way:
```
node do.js <command> <arguments>
```
All commands of `do.js` are described later in this document.

#### Pagination of an Entire Text Corpus
The following shell script generates images of paginated text for the Spanish translation of the _Sense and Sensibility_ novel:
```sh
#!/bin/bash

txt_id=1    # ID of the corpus (obtain like so: node do.js txt-lst)
pg_cnt=999  # number of pages to generate (limit for testing)

w=1680  # screen width [px]
h=1050  # screen height [px]
c=105   # number of characters per line
l=23    # number of lines per page
x=180   # text x-offset [px]
y=260   # text y-offset [px]

node do.js txt-gen-stimuli $txt_id en === $c $l 0 $pg_cnt $pg_cnt $w $h eeeeee 000000 Courier 24 - 1 65 180 260 ""
```
Note that the above code also exemplifies the use of shell variables.  With the screen size, font metrics, and line count and length given as above, the last chapter of the book, Chapter 50, has eight pages and this is the very last one of them (spoiler alert):
```
Por Marianne, sin embargo - a pesar de la descortesía de haber sobrevivido a su pérdida, siempre mantuvo
ese decidido afecto que lo hacía interesarse en todos sus asuntos y que lo llevó a transformarla en su
secreta pauta de perfección femenina; y así, muchas beldades prometedoras terminaron desdeñadas por él
después de algunos días, como sin punto de comparación con la señora Brandon.

La señora Dashwood tuvo la suficiente prudencia de quedarse en la cabaña, sin intentar un traslado a
Delaford; y afortunadamente para sir John y la señora Jennings, en el momento en que se vieron privados
de Marianne, Margaret había llegado a una edad muy apropiada para bailar y que ya podía permitir se le
supusieran enamorados.

Entre Barton y Delaford había esa permanente comunicación que surge naturalmente de un gran cariño
familiar; y de los méritos y las alegrías de Elinor y Marianne, no hay que poner en último lugar el hecho
de que, aunque hermanas y viviendo casi a la vista una de la otra, pudieron hacerlo sin desacuerdos entre
ellas ni producir tensiones entre sus esposos.
```
This is how this text page is rendered as an image:

<kbd>![Sample output: Chapter 50, page 8](media/sample-output-c50p08.png)</kbd>

The `pg_cnt` variable is very useful for testing because it will stop the pagination process after that many pages.  Consequently, it can be used for quick inspection of how the several first pages of a corpus look like, making the experience more interactive.

#### Annotations
This is how a sample annotation (in this case, the first question about Chapter 43, Page 11) looks like when the same settings as above are in effect:

<kbd>![Sample output: Annotation](media/sample-output-c43p11q01.png)</kbd>

#### Arbitrary Text
The following shell script generates an image of subject instructions for the [Lexical Test for Advanced Learners of English (LexTALE)](http://www.lextale.com) task:
```sh
#!/bin/bash

w=1680  # screen width [px]
h=1050  # screen height [px]
c=105   # number of characters per line
l=23    # number of lines per page
x=600   # text x-offset [px]
y=250   # text y-offset [px]

node do.js misc-gen-img ${w}x${h}-instr-lextale $c $l 0 $w $h eeeeee 000000 Helvetica 24 - 1 65 0 $x $y "In this test, you will see 60 words or word-like strings of letters.  Your task is to decide whether this is an existing English word or not.  If you think the presented string is an English word, press the Y key (Y for yes).  If you think it is not an English word, press the N key (N for no).\n\nIf you are certain that it is a word, please respond \"yes\" even if you don't know its exact meaning.  If however you are not sure if it is a word, please respond \"no.\"\n\nPlease note that even non-word strings will be pronounceable in English so do not use that as a criterion when making your choices.\n\nThis is not a timed test and you have as much time as you like for each decision.\n\nIf everything is clear, press Enter to start the test."
```
Below is how the instructions look like as an image:

<kbd>![Sample output: The LEXTALE task instructions](media/sample-output-instr-lextale-a.png)</kbd>

As demonstrated by the above example, proportional font families can be used with no problems.

### Command-Line Utility: Stimulus Schedule
Medusa allows the user to parameterize the way text appears on the resulting images.  For example, text can be rendered easier or more difficult to read from a distance by changing the font size (naturally, larger font will also minimize eye-tracking errors).  As the font size (and other parameters) are being manipulated, the text will be laid out differently across consecutive pages.  This illustrates the pivotal benefit of using Medusa: Irrespective of how the text is laid out across the sequence of pages, the software will automatically associate annotations with appropriate pages.  If that was done by hand, a substantial number of human errors could be introduced into the stimulus schedule.

Medusa generates two tabular schedules of images.  The first one lists only pages of text and no annotations where each row has the following structure:
```
name  type  chapter-num  pg-num  0  0
```
The two trailing zeros were useful for the original research this software was developed for (`do.js` can be edited to remove them).  Below is a real example:
```
title   t  0  0  0  0
c01     t  1  0  0  0
c01p01  t  1  1  0  0
c01p02  t  1  2  0  0
c01p03  t  1  3  0  0
c01p04  t  1  4  0  0
c01p05  t  1  5  0  0
...
```

The second stimulus schedule lists text pages as well.  However, after every annotated page it additionally lists all the annotations for that page.  Each line in that schedule has the following structure:
```
name  type  chapter-num  pg-num  quest-num-chapter  quest-num-pg  quest-id  answer-count  answers-order  answer-correct-num  word-num-a  word-num-b
```
Note that Medusa automatically randomizes the order of answers and that is why that order (`answers-order`) and the ordinal number of the correct answer (`answer-correct-num`) are part of the schedule.  Below is a part of a real example:
```
title      t  0  0  0  0    0  0     .  0    0    0
c01        t  1  0  0  0    0  0     .  0    0    0
c01p01     t  1  1  0  0    0  0     .  0    0    0
c01p01q01  q  1  1  1  1  214  4  3214  3    8   10
c01p01q02  q  1  1  2  2  215  4  4321  4   86   94
c01p01q03  q  1  1  3  3  216  4  2314  3  117  128
c01p01q04  q  1  1  4  4  217  4  4213  3  163  185
c01p01q05  q  1  1  5  5  218  3   321  3  210  216
c01p01q06  q  1  1  6  6  219  4  2134  2  218  225
c01p02     t  1  2  0  0    0  0     .  0    0    0
c01p03     t  1  3  0  0    0  0     .  0    0    0
c01p04     t  1  4  0  0    0  0     .  0    0    0
c01p05     t  1  5  0  0    0  0     .  0    0    0
...
```

Naturally, these schedules can be rearranged and altered to meet the requirements of a particular experiment.

Another output Medusa generates automatically is the tabular list of all annotations.  Below is a part of a real example (with some text pruned for brevity):
```
text-id chapter-id question-id chapter-number page-number page-name question-chapter-num question-page-num question-name question  answer-1-correct answer-2   answer-3 answer-4 answers-order answers-order-correct word-a word-b
      2        109         286              1           1 c01p01                       1                 1 c01p01q01     Where...? Sussex           Berkshire  Surrey   Cornwall 1,2,4,3                           1      8     10
      2        109         287              1           1 c01p01                       2                 2 c01p01q02     How ...?  10               2          5        20       2,1,3,4                           2     86     94
      2        109         288              1           1 c01p01                       3                 3 c01p01q03     Who ...?  Mr...            Mr...      Mr...    No one   2,4,3,1                           4    117    128
      2        109         289              1           1 c01p01                       4                 4 c01p01q04     Why ...?  Out of...        Out of...  They...  They...  2,1,4,3                           2    163    185
      2        109         290              1           1 c01p01                       5                 5 c01p01q05     Has ...?  Yes              No         It is...          3,1,2                             2    210    216
      2        109         291              1           1 c01p01                       6                 6 c01p01q06     How ...?  1                2          3        4        2,3,4,1                           4    218    225
```

The sequential numbers for pages and annotations are generated automatically and match the filenames of rendered images.

### Pushover Notifications
`do.js` contains a function to send [Pushover](https://pushover.net) notifications.  If you'd like to use it, you will need to extend the code and weave in calls to that function where you see fit (e.g., after all chapters have received a certain number of annotations).  Moreover, you will need to add the following to `lib/private.js`:
```sh
var PUSHOVER_USR_KEY = "...",
    PUSHOVER_APP_KEY = "...",
    PUSHOVER_SND     = "pianobar";
    PUSHOVER_DEV     = "phone";
    PUSHOVER_HOST    = "hostname";
```
Often, the shell script will be tested on the local machine but the actual computations will be eventually scheduled on another computer. To prevent the script from sending largely useless and annoying notifications during testing, the script sends them only from the machine with the specified hostname (`PUSHOVER_HOST`). On the receiving end, only the specified device (e.g., a mobile phone; `PUSHOVER_DEV`) will be notified.

### Adapting to New Use Scenarios
While this software focuses on annotating text with questions, the source code can be easily modified to support other types of annotations.  Most small changes will require only a modest understanding of HTML and CSS.  JavaScript and SQL wizardry are necessary only for significant modifications.

### Concurrent Users
Being a Web application, this software can be used by multiple users at the same time.  There are no limits on the number on concurrent connection to the Web server.  This will benefit use cases where multiple text corpora should be annotated at the same time as well as cases with multiple viewers but only a handful of annotators.  However, because there is no built-in access right mechanism, every user can view and edit any text corpus and annotation present in the database.  Consequently, annotators should coordinate amongst themselves to avoid working on the same corpus at the same time.  Moreover, this software does not implement the [model-view-controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) pattern and therefore changes made by other annotators will only be visible after the page has been refreshed.

### Security
This is a special purpose software and, like most artifacts developed for the purpose of academia, it would benefit from more time.  Know then that there is no notion of user accounts, access rights, sign-ups, or sign-ins here.  If you choose to run the HTTP server on a publicly accessible port, you should take regular backups of your database because anyone with the URL of the Web app will be able to use the Web interface much like an annotator would.  Of course, running the server locally (i.e, on your machine), over local area network (LAN; e.g., inside of a research lab), or accessing it through virtual private network (VPN) will prohibit outside access and is the recommended way of using this software.

### Multilingual Support
Medusa works with multilingual text corpora, but only two corpora are provided here as examples at present: The full English and Spanish versions of Jane Austin's _Sense and Sensibility_ novel.

### The Name
In Greek mythology, Medusa was a monster with live snakes in place of hair.  Gazing upon her would turn people to stone.  As an analogy to that, this software petrifies text by turning it into images; once petrified, the image of the text will always look the same.  And besides, it's just a cool-sounding name.  Be warned though, if you are planning on installing Medusa on your computer, you may eventually run into problems with it if you have software named Perseus installed on it as well.


## Dependencies
- [PostgreSQL](https://www.postgresql.org)
- [Node.js](https://nodejs.org)
  * fs, http, moment, path, pg, pg-native, pushover-notifications, readline, request, shelljs, text-table, url
- [ImageMagick](https://www.imagemagick.org)


## Data Structures
Central data structures are defined by the [`lib/ddl.sql`](lib/ddl.sql) script.


## Setup
First, let me note that I've used the Web annotation interface of Medusa on FreeBSD and the command-line utility on MacOS.  Consequently, you may need to show a bit of ingenuity if you run into problems on other operating systems, but I doubt it would anything insurmountable.  Linux will be a piece of cake.  As a general rule beyond this repository and a life lesson in itself, you should avoid using Windows.

### tl;dr
First, install [PostgreSQL](https://www.postgresql.org), [Node.js](https://nodejs.org), and [ImageMagick](https://www.imagemagick.org) and start Postgres.  Then, execute:
```sh
prj_root=~/prj
mkdir -p $prj_root
cd $prj_root

git clone https://gitlab.com/ubiq-x/medusa
cd medusa

sudo npm i -g npm
npm install fs http moment path pg pg-native pushover-notifications readline request shelljs text-table url

node do.js db-init
psql -U postgres -d medusa -a -f data/sense-and-sensibility-en-test.sql  # populate with test data

node do.js server-start
```
The Web interface will be available at [`http://localhost:9999/index.html`](http://localhost:9999/index.html).

### Node.js
The command-line utility `do.js` contains JavaScript code that can be executed by Node.js.  That utility is a central component of this software.  It is used to initialize the persistence layer (i.e., the database), serve the Web app used for annotation, paginate text corpora, and render images.  In order for it to work, you must first install [Node.js](https://nodejs.org) and then install the required modules:
```
sudo npm i -g npm
npm install fs http moment path pg pg-native pushover-notifications readline request shelljs text-table url
```

### Database Server and The Database
The database is used to store text corpora along with all annotations made against them.  You will need [PostgreSQL](https://www.postgresql.org) for that.  Once you have PostgreSQL up and running, enter the database credentials to `lib/private.js`:
```sh
var DB_CONN_STR = "postgres://<user>:<passwd>@<host>";
```
For example, if you want to use user `postgres`:
```sh
var DB_CONN_STR = "postgres://psql:DisP@SSwordSoStronk@localhost";
```
Or if you develop on MacOS but deploy to FreeBSD:
```sh
var DB_CONN_STR = "postgres://" + (os.type() === "Darwin" ? "postgres" : "pgsql") + ":DisP@SSwordSoStronk@localhost";
```
Then, initialize the database like so:
```
node do.js db-init
```
The above command will remove the database if it already exists so it can also be used to start with a clean slate any time you want.  If however all you need is to wipe the contents of the database, run this instead:
```
node do.js db-wipe
```
Both commands ask for a confirmation so no unpleasant surprises await you.

The database schema can be found in [`lib/ddl.sql`](lib/ddl.sql).

### Web Server
The HTTP server which serves the text annotation client can be started like so:
```
node do.js server-start
```
The default port is 9999, but it can be changed by editing the `CONST` section of `do.js`.  To access the Web interface navigate your browser to `http://<host>:<port>/index.html` (e.g., [`http://localhost:9999/index.html`](http://localhost:9999/index.html) if the server is running on the local machine).  Once running, the server can be killed with `CTRL+C`.

All HTTP server activity is logged in `log/http.log`.

### Adding and Removing Text Corpora
At the moment, adding and removing text corpora can only be done by execute SQL queries against the DBMS.  These operations are infrequent so this is much less of a hussle than it may seem and is done like so:
```
psql -U <user> -d medusa -a -f <text.sql>
```
Sample text corpora are located inside the `data` directory.  I would encourage using these examples as templates because they demonstrate the use of good practices such as transactions which protect data integrity through a set of properties called [ACID](https://en.wikipedia.org/wiki/ACID).

The best way of handling backups is by backing up the Postgres `medusa` database itself.

### ImageMagik
Medusa uses ImageMagik to render images.  It isn't needed for annotating text.


## Command Reference
As I have indicated above, `do.js` can be used to initialize and wipe the database as well as to start the Web server.  However, it has other useful commands as well.  Below is a complete list of those commands, most of which take no arguments.  Closer inspection of `do.js` will reveal that not all function (e.g., `pushover()`) are accessible thought the command-line interface.  While there aren't many function like that, this represents the "under-development" nature of this project.

- **`db-init`**  
  Initialize the database (asks for confirmation).

- **`db-wipe`**  
  Wipe the databse clean (asks for confirmation).

- **`misc-gen-img`**  
  Generate an image from an arbitrary text.

  Positional arguments:
  - `fname` Filename (without the extension which defaults to `png`) [str]
  - `line-len` Line length in character spaces [int]
  - `page-line-cnt` Page length in lines [int]
  - `do-print-diag-inf` Print diagnostic information? [{0,1}]
  - `img-w` Image width in pixels [int]
  - `img-h` Image height in pixels [int]
  - `img-bg-color` Image background color [str]
  - `img-font-color` Image font color [str]
  - `img-font-family` Image font family [str]
  - `img-font-size` Image font size [int]
  - `img-font-weight` Image font weight (integer or '-')
  - `img-char-space` Image character spacing [int]
  - `img-line-space` Image line spacing [int]
  - `img-txt-offset-x` Image text top-left corner x-offset [int]
  - `img-txt-offset-y` Image text top-left corner y-offset [int]
  - `img-txt` Text to appear on the image [str]

  Example:
  ```
  node do.js misc-gen-img test 105 25 0 1680 1050 eeeeee 000000 Courier 24 - 1 65 180 250 "blah blah\nblah"
  ```

  All output is saved inside the `stim` directory (short for _stimuli_).

- **`ping`**  
  Prints `pong`.

- **`question-lst`**  
  List all questions for the specified chapter.  This command isn't typically run from the command-line for reasons other than debugging.

  Positional arguments:
  - `chapter-id`: ID of the chapter [int]

- **`server-start`**  
  Start the HTTP server.

- **`txt-add`**  
  Add text corpus. [Not implemented yet]

- **`txt-gen-stimuli`**  
  Generate images of text and all annotations for the specified text corpus.  It also generates a text file with the text itself.  The text in that file is divided into pages exactly as it appears on the images.

  Positional arguments:
  - `txt-id` ID of the text corpus [int]
  - `lang` Language, e.g. `en` [str]
  - `page-sep` Page separator for the text file [str]
  - `line-len` Line length in character spaces [int]
  - `page-line-cnt` Page length in lines [int]
  - `do-print-diag-inf` Print diagnostic information? [{0,1}]
  - `content-img-cnt-max` Number of text images; 0=all [int]
  - `question-img-cnt-max` Number of question images; 0=all [int]
  - `img-w` Image width in pixels [int]
  - `img-h` Image height in pixels [int]
  - `img-bg-color` Image background color [str]
  - `img-font-color` Image font color [str]
  - `img-font-family` Image font family [str]
  - `img-font-size` Image font size [int]
  - `img-font-weight` Image font weight (integer or '-')
  - `img-char-space` Image character spacing [int]
  - `img-line-space` Image line spacing [int]
  - `img-txt-offset-x` Image text top-left corner x-offset [int]
  - `img-txt-offset-y` Image text top-left corner y-offset [int]
  - `img-chapter-subtxt` Text to appear on all chapter pages under chapter title [str]

  Example:
  ```
  node do.js txt-gen-stimuli 8 en === 105 25 0 1 1 1600 1200 eeeeee 000000 Courier 24 - 1 65 180 260 ""
  ```

  All output is saved inside the `stim` directory (short for _stimuli_).

- **`txt-lst`**  
  List all text corpora along with their IDs which are used for image stimuli generation.

- **`txt-rem`**  
  Remove text corpus. [Not implemented yet]


## Acknowledgments
- I would like to thank Beatriz Barragan for being the guardian of quality for the Spanish translation of _Sense and Sensibility_.
- I worked as a post-doctoral research associate at the University of Pittsburgh when I developed this software.


## Citing
Loboda, T.D. (2018).  Medusa: Text Corpora Annotation and Stimuli Generation [computer software].  Available at _https://gitlab.com/ubiq-x/medusa_


## License
This project is licensed under the [BSD License](LICENSE.md).
